# Content Access Lite

Provides a simple widget for the [Content Access](https://www.drupal.org/project/content_access) module to switch node view permissions for anonymous users on or off.

It integrates into the Content Access module in such a way that it could be installed or uninstalled without affecting the content access settings. Content Access settings could still be set via the default settings form if permissions are granted.

There is one small difference to Content Access: If the settings set in the content form match the default settings, there will be nothing saved to the content_access table, as it is not necessary. If a record already exists there, it will be removed.

https://www.drupal.org/sandbox/osopolar/2879872
